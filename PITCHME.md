# DSR Platform
## Create knowledge together

---

## Motivation

- DSR students and teachers have created valuable content and projects, but where are they? 
- Scalability of teaching needs standardization

---

## Our Options

- GitHub
- GitLab
- Dropbox

---

## Results

- GitHub 
	+ Students: showcase technical or coding skills
	+ Teachers: content is already there
- **GitLab**
    + Private repositories
	+ Groups and permissions
	+ Option to import/mirror GitHub repos
	+ Scalability	
- Dropbox

---

## What to do now?

- Encourage students to use GitHub
- Creating first version of each masterclass.

--- 

## Structure

| **GitHub** | **GitLab**| **DSR**                               |
|------------|-----------|---------------------------------------|
|            | Group     | 3 groups: Classes, teachers, participants |
| Repo       | Project   | Masterclass                           |
| Folder     | Folder    | Set of slides                         |

---

## Potential use of different options

- Gist/Snippet: Specific questions
- Issues, Todos, Milestones (GitLab): Track projects of students
- Pull request: Peer review, errors in code
- Integration with Slack: Creation of issues and pull requests
- Wiki: General information of each class, additional links, Other exercises to practice
- Exercises: [nbgrader](https://github.com/jupyter/nbgrader)

---

## nbgrader

1. Create assignment (Instructions & Solutions)
2. Generate student version
3. Grade submissions
4. Generate feedback

